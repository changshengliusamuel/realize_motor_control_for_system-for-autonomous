#define BRAKE 0
#define CW    1
#define CCW   2
#define CS_THRESHOLD 15   // Definition of safety current (Check: "1.3 Monster Shield Example").

//MOTOR 1
#define MOTOR_A1_PIN 7
#define MOTOR_B1_PIN 8

//MOTOR 2
#define MOTOR_A2_PIN 4
#define MOTOR_B2_PIN 9

#define PWM_MOTOR_1 5
#define PWM_MOTOR_2 6

#define CURRENT_SEN_1 A2
#define CURRENT_SEN_2 A3

#define EN_PIN_1 A0
#define EN_PIN_2 A1

#define MOTOR_1 0
#define MOTOR_2 1

short usSpeed = 150;  //default motor speed,max_speed is 255
unsigned short usMotor_Status_for_motor_A = BRAKE;
unsigned short usMotor_Status_for_motor_B = BRAKE;
 
/*****************sensor area****************/
#define sensor_A 10
#define sensor_B 11
#define sensor_C 12
#define sensor_D 13
/********************************************/
void setup()                         
{
  pinMode(MOTOR_A1_PIN, OUTPUT);
  pinMode(MOTOR_B1_PIN, OUTPUT);

  pinMode(MOTOR_A2_PIN, OUTPUT);
  pinMode(MOTOR_B2_PIN, OUTPUT);

  pinMode(PWM_MOTOR_1, OUTPUT);
  pinMode(PWM_MOTOR_2, OUTPUT);

  pinMode(CURRENT_SEN_1, OUTPUT);
  pinMode(CURRENT_SEN_2, OUTPUT);  

  pinMode(EN_PIN_1, OUTPUT);
  pinMode(EN_PIN_2, OUTPUT);

  pinMode(sensor_A, INPUT);
  pinMode(sensor_B, INPUT);
  pinMode(sensor_C, INPUT);
  pinMode(sensor_D, INPUT);
}

void loop() 
{  
  digitalWrite(EN_PIN_1, HIGH);//?
  digitalWrite(EN_PIN_2, HIGH); //?
  //start
  if (digitalRead(sensor_B) == 1 && digitalRead(sensor_C) == 0 && digitalRead(sensor_A) == 0 && digitalRead(sensor_D) == 0)//直走
  {
      Forward(MOTOR_1);
      Forward(MOTOR_2);
  }
  if (digitalRead(sensor_B) == 1 && digitalRead(sensor_C) == 0 && digitalRead(sensor_A) == 1 && digitalRead(sensor_D) == 0)//右轉
  {
      delay(500);
      Forward(MOTOR_1);
      Reverse(MOTOR_2);
      while(1)
      {
        if (digitalRead(sensor_B) == 1 && digitalRead(sensor_C) == 0 && digitalRead(sensor_A) == 0 && digitalRead(sensor_D) == 0)//直向
        {
          break;
        }
      }
  }
  if (digitalRead(sensor_B) == 1 && digitalRead(sensor_C) == 1 && digitalRead(sensor_A) == 1 && digitalRead(sensor_D) == 1)//左轉
  {
      delay(500);
      Forward(MOTOR_2);
      Reverse(MOTOR_1);
      while(1)
      {
        if (digitalRead(sensor_B) == 1 && digitalRead(sensor_C) == 0 && digitalRead(sensor_A) == 0 && digitalRead(sensor_D) == 0)//直向
        {
          break;
        }
      }
  }
  


     
      
  
}

void Stop()
{
  usMotor_Status_for_motor_A = BRAKE;
  usMotor_Status_for_motor_B = BRAKE;
  motorGo(MOTOR_1, usMotor_Status_for_motor_A, 0);
  motorGo(MOTOR_2, usMotor_Status_for_motor_B, 0);
}

void Forward(int motor_name)
{
  if(motor_name==MOTOR_1)
  {
    usMotor_Status_for_motor_A = CW;
    motorGo(motor_name, usMotor_Status_for_motor_A, usSpeed);
  }
  else if(motor_name==MOTOR_2)
  {
    usMotor_Status_for_motor_B = CW;
    motorGo(motor_name, usMotor_Status_for_motor_B, usSpeed); 
  }
}

void Reverse(int motor_name)
{
  if(motor_name==MOTOR_1)
  {
    usMotor_Status_for_motor_A = CCW;
    motorGo(motor_name, usMotor_Status_for_motor_A, usSpeed);
  }
  else if(motor_name==MOTOR_2)
  {
    usMotor_Status_for_motor_B = CCW;
    motorGo(motor_name, usMotor_Status_for_motor_B, usSpeed); 
  }
}

void IncreaseSpeed()
{
  usSpeed = usSpeed + 10;
  if(usSpeed > 255)
  {
    usSpeed = 255;  
  }
  motorGo(MOTOR_1, usMotor_Status_for_motor_A, usSpeed);
  motorGo(MOTOR_2, usMotor_Status_for_motor_B, usSpeed);  
}

void DecreaseSpeed()
{
  usSpeed = usSpeed - 10;
  if(usSpeed < 0)
  {
    usSpeed = 0;  
  }
  motorGo(MOTOR_1, usMotor_Status_for_motor_A, usSpeed);
  motorGo(MOTOR_2, usMotor_Status_for_motor_B, usSpeed);  
}

void motorGo(uint8_t motor, uint8_t direct, uint8_t pwm)         //Function that controls the variables: motor(0 ou 1), direction (cw ou ccw) e pwm (entra 0 e 255);
{
  if(motor == MOTOR_1)
  {
    if(direct == CW)
    {
      digitalWrite(MOTOR_A1_PIN, LOW); 
      digitalWrite(MOTOR_B1_PIN, HIGH);
    }
    else if(direct == CCW)
    {
      digitalWrite(MOTOR_A1_PIN, HIGH);
      digitalWrite(MOTOR_B1_PIN, LOW);      
    }
    else
    {
      digitalWrite(MOTOR_A1_PIN, LOW);
      digitalWrite(MOTOR_B1_PIN, LOW);            
    }
    
    analogWrite(PWM_MOTOR_1, pwm); 
  }
  else if(motor == MOTOR_2)
  {
    if(direct == CW)
    {
      digitalWrite(MOTOR_A2_PIN, LOW);
      digitalWrite(MOTOR_B2_PIN, HIGH);
    }
    else if(direct == CCW)
    {
      digitalWrite(MOTOR_A2_PIN, HIGH);
      digitalWrite(MOTOR_B2_PIN, LOW);      
    }
    else
    {
      digitalWrite(MOTOR_A2_PIN, LOW);
      digitalWrite(MOTOR_B2_PIN, LOW);            
    }
    
    analogWrite(PWM_MOTOR_2, pwm);
  }
}


